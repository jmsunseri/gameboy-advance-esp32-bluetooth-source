#include "BluetoothA2DPSource.h"
#include "AudioTools.h"
#include "esp_adc_cal.h"

constexpr size_t BUFFER_SIZE = 15;
constexpr size_t RAW_MUSIC_SIZE = 18000;

AnalogAudioStream adc;
BluetoothA2DPSource a2dp_source;

char ssid[12];
int16_t touch_threshold = 90;
uint16_t minutes = 5;
unsigned long shutdown_ms;

// multi sampling
int16_t buffer[2][BUFFER_SIZE] = {{0}, {0}};
int bufferIterator[2] = {0, 0};
int rawMusicIterator = 0;

int16_t getAverage(int16_t raw, int channel)
{
  int16_t sum = 0;
  buffer[channel][bufferIterator[channel]] = raw;

  for (int i = 0; i < BUFFER_SIZE; i++)
  {
    sum += buffer[channel][i];
  }

  bufferIterator[channel] += 1;
  if (bufferIterator[channel] == BUFFER_SIZE)
  {
    bufferIterator[channel] = 0;
  }

  return (sum / BUFFER_SIZE);
}

// callback used by A2DP to provide the sound data
int32_t get_sound_data(Frame *frames, int32_t frameCount)
{
  uint8_t *data = (uint8_t *)frames;
  int frameSize = 4;
  size_t resultBytes = adc.readBytes(data, frameCount * frameSize);

  // for (int i = 0; i < frameCount; ++i)
  // {
  //   frames[i].channel1 = getAverage(frames[i].channel1, 0);
  //   frames[i].channel2 = getAverage(frames[i].channel2, 1);
  // }

  delay(1);
  return resultBytes / frameSize;
}


void muteSpeaker()
{
  pinMode(GPIO_NUM_25, OUTPUT_OPEN_DRAIN);
  Serial.println("State changed to connected setting pin 25 to output");
  blink(2, false);
}

void turnOnSpeaker()
{
  pinMode(GPIO_NUM_25, INPUT);
  Serial.println("State changed to disconnected setting pin 25 to input");
  blink(2, true);
}

bool isValid(const char *id, esp_bd_addr_t address, int rssi)
{
  return true;
}

void touchCallback()
{
  Serial.println("I've been touched");
}

void on_connection_state_changed(esp_a2d_connection_state_t state, void *)
{

  if (state == esp_a2d_connection_state_t::ESP_A2D_CONNECTION_STATE_CONNECTED)
  {
    Serial.println(a2dp_source.to_str(state));
    shutdown_ms = 0;
    muteSpeaker();
  }
  else if (state == esp_a2d_connection_state_t::ESP_A2D_CONNECTION_STATE_DISCONNECTED)
  {
    if (shutdown_ms == 0)
    {
      Serial.println(a2dp_source.to_str(state));
      shutdown_ms = millis() + 1000 * 60 * minutes;
      turnOnSpeaker();
    }
  }
}

void blink(int32_t times, bool fast)
{
  if (times >= 1)
  {
    digitalWrite(GPIO_NUM_13, HIGH);
    delay(fast ? 250 : 500);
    digitalWrite(GPIO_NUM_13, LOW);
    delay(fast ? 250 : 500);
    blink(times - 1, fast);
  }
}

void setup()
{
  Serial.begin(115200);

  pinMode(GPIO_NUM_25, INPUT); // used to bring the P35 point to ground when BT connected
  pinMode(GPIO_NUM_13, OUTPUT);
  blink(1, true);
  shutdown_ms = millis() + 1000 * 60 * minutes;
  // start i2s input with default configuration
  Serial.println("starting I2S-ADC...");
  adc.begin(adc.defaultConfig(RX_MODE));
  snprintf(ssid, 12, "Gameboy-%08llX", ESP.getEfuseMac());
  a2dp_source.set_local_name(ssid);
  Serial.print("bluetooth device name ");
  Serial.println(ssid);
  a2dp_source.set_volume(30);
  a2dp_source.set_on_connection_state_changed(on_connection_state_changed);

  a2dp_source.set_ssid_callback(isValid);
  a2dp_source.set_auto_reconnect(false);
  a2dp_source.start(get_sound_data);
  touchAttachInterrupt(GPIO_NUM_33, touchCallback, touch_threshold);
  esp_sleep_enable_touchpad_wakeup();
}

void loop()
{
  delay(1000);
  if (millis() > shutdown_ms && shutdown_ms != 0)
  {
    Serial.println("taking a nap");
    blink(5, false);
    esp_deep_sleep_start();
  }
}
